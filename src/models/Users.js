const mongoose = require('mongoose');
const Joi = require('joi');

const joiShema = Joi.object({
  email: Joi.string().email({
    minDomainSegments: 2, 
    tlds: { allow: ['com', 'net'] }
  })
  .required()
  .messages({
    "any.required": `Email is required`
  }),
  password: Joi.string()
  .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
  .required()
  .messages({
    "string.pattern.base": `Password must be between 3 to 30 characters and contain letters or numbers`,
    "string.empty": `Password can not be empty`,
    "any.required": `Password is required`,
  }),
  role: Joi.string()
  .required()
  .messages({
    "any.required": `Role is required`,
    "string.empty": `Role cannot be an empty`
  }),
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true
  },
  role: {
    type: String,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = {
  User,
  joiShema
}