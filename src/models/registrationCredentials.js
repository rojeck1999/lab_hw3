const mongoose = require('mongoose');

const RegCredential = mongoose.model('regCredential', {
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String
  },
  role: {
    type: String,
    required: true
  }
})

module.exports = {
  RegCredential
}