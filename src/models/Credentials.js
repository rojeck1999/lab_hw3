const mongoose = require('mongoose');

const Credential = mongoose.model('Credential', {
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String
  }
})

module.exports = {
  Credential
}