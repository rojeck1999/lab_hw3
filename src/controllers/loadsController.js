const {saveLoad, findAndPostLoad, findLoad, findShippedLoad, findActiveLoad, findAndReturnLoads, findAndUpdateLoad, findAndDeleteLoad, updateLoadState} = require('../services/loadsService.js');
const {Truck} = require('../models/Trucks.js');

const createLoad = async (req, res, next) => {
  const {name, payload, pickup_address, delivery_address} = req.body;
  const width = req.body.dimensions.width;
  const length = req.body.dimensions.length;
  const height = req.body.dimensions.height;
  const role = req.user.role;
  const userId = req.user.userId;
  if(role === 'SHIPPER') {
    await saveLoad({name, payload, pickup_address, delivery_address, width, length, height, userId});
    res.status(200).json({"message": "Load created successfully"})
  } else {
    res.status(400).json({"message": "You cannot create loads. Please login as a shipper"})
  }
}

const postLoad = async (req, res, next) => {
  const loadId = req.params.id;
  const role = req.user.role;
  if (role === 'SHIPPER') {
    const result = await findAndPostLoad(loadId);
    if(result) {
      res.status(200).send({"message": "Load posted successfully", "driver_found": result})
    } else {
      res.status(200).send({"message": "Driver was not found", "driver_found": result})
    }
  } else {
    res.status(400).json({"message": "You cannot post the load. Please login as a shipper"})
  }
}

const getLoad = async (req, res, next) => {
  const loadId = req.params.id;
  const role = req.user.role;
  if (role === 'SHIPPER') {
    const load = await findLoad(loadId);
    res.status(200).json({"load": load})
  } else {
    res.status(400).json({"message": "You cannot view the load. Please login as a shipper"})
  }
}

const getShippedLoad = async (req, res, next) => {
  const loadId = req.params.id;
  const role = req.user.role;
  if (role === 'SHIPPER') {
    const {load, truck} = await findShippedLoad(loadId);
    if(truck) {
      res.status(200).json({"load": load, "truck": truck})
    } else {
      res.status(400).json({"message": "This load is not assigned to a truck"})
    }
  } else {
    res.status(400).json({"message": "You cannot view the load. Please login as a shipper"})
  }
}

const getActiveLoad = async (req, res, next) => {
  const userId = req.user.userId;
  const role = req.user.role;
  if(role === 'DRIVER') {
    const result = await findActiveLoad(userId);
    if (result) {
      res.status(200).json({"load": result})
    } else {
      res.status(400).json({"message": "You don't have an active load"})
    }
  } else {
    res.status(400).json({"message": "You cannot view the active load. Please login as a driver"})
  }
}

const getLoads = async (req, res, next) => {
  const limit = (req.query.limit) ? req.query.limit : 10;
  const offset = (req.query.offset) ? req.query.offset : 0;
  const status = req.query.status;
  const role = req.user.role;
  const userId = req.user.userId;
  const loads = await findAndReturnLoads({status, limit, offset, role, userId});
  res.status(200).json({"loads": loads})
}

const updateLoad = async (req, res, next) => {
  const loadId = req.params.id;
  const {name, payload, pickup_address, delivery_address} = req.body;
  const width = req.body.dimensions.width;
  const length = req.body.dimensions.length;
  const height = req.body.dimensions.height;
  const role = req.user.role;
  if (role === 'SHIPPER') {
    const result = await findAndUpdateLoad({loadId, name, payload, pickup_address, delivery_address, width, length, height});
    if(result) {
      res.status(200).json({"message": "Load details changed successfully"})
    } else {
      res.status(400).json({"message": "You cannot update the active load"})
    }
  } else {
    res.status(400).json({"message": "You cannot update the load. Please login as a shipper"})
  }
}

const deleteLoad = async (req, res, next) => {
  const loadId = req.params.id;
  const role = req.user.role;
  if (role === 'SHIPPER') {
    const result = await findAndDeleteLoad(loadId);
    if (result) {
      res.status(200).json({"message": "Load deleted successfully"})
    } else {
      res.status(400).json({"message": "You cannot delete the active load"})
    }
  }else {
    res.status(400).json({"message": "You cannot delete the load. Please login as a shipper"})
  }
}

const updateLoadStateForTruck = async (req, res, next) => {
  const role = req.user.role;
  const userId = req.user.userId;
  const trucks = await Truck.find();
  const truck = trucks.find(truck => {
    if(truck.assigned_to === userId) {
      return truck;
    }
  })
  if(role === 'DRIVER') {
    const result = await findActiveLoad(userId);
    if (result) {
      const message = await updateLoadState(result, truck);
      res.status(200).json({"message": `Load state changed to ${message}`})
    } else {
      res.status(400).json({"message": "You don't have an active load"})
    }
  } else {
    res.status(400).json({"message": "You cannot change state of the load. Please login as a driver"})
  }
}

module.exports = {
  createLoad,
  postLoad,
  getLoad,
  getShippedLoad,
  getActiveLoad,
  getLoads,
  updateLoad,
  deleteLoad,
  updateLoadStateForTruck
}