const {User} = require('../models/Users.js');
const {RegCredential} = require('../models/registrationCredentials.js');
const {deleteShipperAccount, deleteDriverAccount, changeUserPassword} = require('../services/usersService.js');
const bcrypt = require('bcryptjs');

const getUser = (req, res, next) => {
  User.findById({_id: req.user.userId})
    .then(result => {
      res.status(200).send(result)
    })
}

const deleteUser = async (req, res, next) => {
  const {email, userId, role} = req.user;
  if(role === 'DRIVER') {
    const result = await deleteDriverAccount({email, userId});
    if (result) {
      res.status(200).json({"message": "Profile deleted successfully"})
    } else {
      res.status(400).json({"message": "You can not delete your account, you have active load"})
    }
  }
  if(role === 'SHIPPER') {
    const result = await deleteShipperAccount({email, userId});
    if(result) {
      res.status(200).json({"message": "Profile deleted successfully"})
    }else {
      res.status(400).json({"message": "You cannot delete your account while you have an active load"})
    }
  }
}

const changePassword = async (req, res, next) => {
  const email = req.user.email;
  const {oldPassword, newPassword} = req.body;
  const regcredential = await RegCredential.findOne({email: email});
  if(await bcrypt.compare(String(oldPassword), String(regcredential.password))) {
    await changeUserPassword({email, oldPassword, newPassword});
    res.status(200).json({"message": "Password changed successfully"});
  } else {
    res.status(400).json({'message': 'Please enter correct password'})
  }
}

module.exports = {
  getUser,
  deleteUser,
  changePassword
}