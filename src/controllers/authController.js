const {User, joiShema} = require('../models/Users.js');
const {Credential} = require('../models/Credentials.js');
const bcrypt = require('bcryptjs');
const jsonwebtoken = require('jsonwebtoken');
const {saveUser, checkPassword, createAndSendMail} = require('../services/authService.js');
const nodemailer = require('nodemailer');

const registerUser = async (req, res, next) => {
  const {email, role, password} = req.body;
  const validationResult = joiShema.validate({email, role, password}, { abortEarly: false });
  try {
    if(!validationResult.error) {
      await saveUser({role, email, password});
      return await res.status(200).json({"message": "Profile created successfully"})
    } else {
      res.status(400).json({"message": validationResult.error.message});
    }
  } catch (err) {
    res.send(err.message);
  }
}

const loginUser = async (req, res, next) => {
  const {email, password} = req.body;
  const jwtToken = await checkPassword({email, password});
  if(jwtToken) {
    return await res.status(200).json({"jwt_token": jwtToken})
  } else {
    return await res.status(400).json({'message': 'Not authorized'})
  }
}

const sendPassword = async (req, res, next) => {
  const email = req.body.email;
  const user = await User.findOne({email: email});
  if(user) {
    await createAndSendMail(email);
    return res.status(200).json({"message": "New password sent to your email address"})
  } else {
     return res.status(400).json({'message': `User with ${email} does not exist`})
  }
}

module.exports = {
  registerUser,
  loginUser,
  sendPassword
}