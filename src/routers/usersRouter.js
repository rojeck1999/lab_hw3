const express = require('express');
const router = express.Router();
const {getUser, deleteUser, changePassword} = require('../controllers/usersController.js');
const {authMiddleware} = require('../middleware/authMiddleware.js');

router.get('/me', authMiddleware, getUser);
router.delete('/me', authMiddleware, deleteUser);
router.patch('/me/password', authMiddleware, changePassword)

module.exports = {
  usersRouter: router
}