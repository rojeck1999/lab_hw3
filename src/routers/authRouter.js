const express = require('express');
const router = express.Router();
const {registerUser, loginUser, sendPassword} = require('../controllers/authController.js');

router.post('/register', registerUser);
router.post('/login', loginUser);
router.post('/forgot_password', sendPassword)

module.exports = {
  authRouter: router,
}