const express = require('express');
const router = express.Router();
const {createLoad, postLoad, getLoad, getShippedLoad, getActiveLoad, getLoads, updateLoad, deleteLoad, updateLoadStateForTruck} = require('../controllers/loadsController.js');

router.post('/', createLoad);
router.post('/:id/post', postLoad);
router.get('/active', getActiveLoad);
router.get('/:id', getLoad);
router.get('/:id/shipping_info', getShippedLoad);
router.get('/', getLoads);
router.put('/:id', updateLoad);
router.delete('/:id', deleteLoad);
router.patch('/active/state', updateLoadStateForTruck)


module.exports = {
  loadsRouter: router
}