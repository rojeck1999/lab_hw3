const mongoose = require('mongoose');
const {Load} = require('../models/Loads.js');
const {Truck} = require('../models/Trucks.js');

const saveLoad = async ({name, payload, pickup_address, delivery_address, width, length, height, userId}) => {
  const load = new Load ({
    created_by: userId,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions: {
      width,
      length,
      height
    }
  });
  load.save();
}

const findAndPostLoad = async (loadId) => {
  const load = await Load.findById(loadId);
  load.status = 'POSTED';
  await load.save();
  const fitTruck = await findDriver(load);
  if(fitTruck) {
    fitTruck.status = 'OS';
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.assigned_to = fitTruck.assigned_to;
    load.logs = {
      "message" :`Load assigned to driver with id ${fitTruck.created_by}`,
      "time": Date.now()
    }
    await fitTruck.save();
    await load.save();
    return true;
  } else {
    load.status = 'NEW';
    load.logs = {
      "message" :`Driver was not found`,
      "time": Date.now()
    }
    await load.save();
    return false;
  }
}

const findDriver = async (load) => {
  const trucksArray = await Truck.find();
  const loadWidth = load.dimensions.width;
  const loadLength = load.dimensions.length;
  const loadHeight = load.dimensions.height;
  const loadWeight = load.payload;

  const fitTruck = trucksArray.find(truck => {
    if(truck.status === 'IS' && truck.dimensions.width > loadWidth
      && truck.dimensions.length > loadLength && truck.dimensions.height > loadHeight
      && truck.dimensions.payload > loadWeight && truck.assigned_to) {
        return truck;
    }
  });
  return fitTruck;
}

const findLoad = async (loadId) => {
  return await Load.findById(loadId);
}

const findShippedLoad = async (loadId) => {
  const load = await Load.findById(loadId);
  const driverId = load.assigned_to;
  const truck = await Truck.findOne({'assigned_to': driverId});
  return {load, truck};
}

const findActiveLoad = async (userId) => {
  const loads = await Load.find();
  const activeLoad = loads.find(load => {
    let driverId = load.assigned_to;
    if (userId === driverId) {
      return load;
    }
  })
  return activeLoad;
}

const findAndReturnLoads = async({status, limit, offset, role, userId}) => {
  if(role === 'SHIPPER') {
    if(status) {
      return await Load.aggregate()
      .match({'created_by': mongoose.Types.ObjectId(userId)})
      .match({'status': status})
      .limit(limit)
      .skip(offset);
    } else {
      console.log(typeof userId);
      return await Load.aggregate()
      .match({'created_by': mongoose.Types.ObjectId(userId)})
      .limit(limit)
      .skip(offset);
    }
  }
  if(role === 'DRIVER') {
    if(status) {
      return await Load.aggregate()
      .match({'status': status})
      .limit(limit)
      .skip(offset);
    } else {
      return await Load.aggregate()
      .limit(limit)
      .skip(offset);
    }
  }
}

const findAndUpdateLoad = async ({loadId, name, payload, pickup_address, delivery_address, width, length, height}) => {
  const load = await Load.findById(loadId);
  if (load.status === 'NEW') {
    load.name = name;
    load.payload = payload;
    load.pickup_address = pickup_address;
    load.delivery_address = delivery_address;
    load.dimensions.width = width;
    load.dimensions.length = length;
    load.dimensions.height = height;
    return await load.save();
  }
}

const findAndDeleteLoad = async (loadId) => {
  const load = await Load.findById(loadId);
  if(load.status === 'NEW') {
    return await load.deleteOne();
  }
}

const updateStateOfLoad = async (load, truck) => {
  switch (load.state) {
    case ('En route to Pick Up'):
      await load.updateOne({
        state: 'Arrived to Pick Up',
        $push: {
          logs: {
            message: "Load state changed to 'Arrived to Pick Up'",
            time: Date.now()
          }
        }
      });
      return 'Arrived to Pick Up';
    case ('Arrived to Pick Up'):
      await load.updateOne({
        state: 'En route to delivery',
        $push: {
          logs: {
            message: "Load state changed to 'En route to delivery'",
            time: Date.now()
          }
        }
      });
      return 'En route to delivery';
    case ('En route to delivery'):
      await load.updateOne({
        state: 'Arrived to delivery',
        status: 'SHIPPED',
        $push: {
          logs: {
            message: "Load state changed to 'Arrived to delivery'",
            time: Date.now()
          }
        }
      });
      await truck.updateOne({
        status: 'IS',
        assigned_to: null
      });
      return 'Arrived to delivery';
  }
}

module.exports = {
  saveLoad,
  findAndPostLoad,
  findLoad,
  findShippedLoad,
  findActiveLoad,
  findAndReturnLoads,
  findAndUpdateLoad,
  findAndDeleteLoad,
  updateStateOfLoad
}