const {User} = require('../models/Users.js');
const {Credential} = require('../models/Credentials.js');
const {RegCredential} = require('../models/registrationCredentials.js');
const {Truck} = require('../models/Trucks.js');
const {Load} = require('../models/Loads.js');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const deleteShipperAccount = async ({email, userId}) => {
  const load = await Load.aggregate()
    .match({'status': 'ASSIGNED'});
  if(load.length === 0) {
    await User.findByIdAndDelete(userId);
    await Credential.findOneAndDelete(email);
    await RegCredential.findOneAndDelete(email);
    await Load.deleteMany({'created_by': mongoose.Types.ObjectId(userId)})
  }
}

const deleteDriverAccount = async({email, userId}) => {
  const truck = await Truck.aggregate()
  .match({'assigned_to': userId});
  if(truck.length === 0) {
    await User.findByIdAndDelete(userId);
    await Credential.findOneAndDelete(email);
    await RegCredential.findOneAndDelete(email);
    await Truck.deleteMany({'created_by': mongoose.Types.ObjectId(userId)})
  }
}

const changeUserPassword = async ({email, newPassword}) => {
  const credential = await Credential.findOne({email: email});
  const regCredential = await RegCredential.findOne({email: email});
  const changePassword = await bcrypt.hash(newPassword, 10);
  regCredential.password = changePassword;
  credential.password = newPassword;
  await credential.save();
  await regCredential.save()
}

module.exports = {
  deleteShipperAccount,
  deleteDriverAccount,
  changeUserPassword
}