const {User} = require('../models/Users.js');
const {Credential} = require('../models/Credentials.js');
const {RegCredential} = require('../models/registrationCredentials.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const SECRET_KEY = 'alwaysclose';

const saveUser = async ({role, email, password}) => {
  const user = new User({
    role,
    email
  });

  const regCredential = new RegCredential({
    role,
    email,
    password: await bcrypt.hash(password, 10)
  });

  const credential = new Credential({
    email,
    password
  });
  await regCredential.save();
  await credential.save();
  await user.save();
}

const checkPassword = async ({email, password}) => {
  const user = await User.findOne({email: email});
  const credential = await RegCredential.findOne({email: email});
  if(user && bcrypt.compare(String(credential.password), String(password))) {
    const payload = {email: user.email, userId: user._id, role: user.role};
    return jwt.sign(payload, SECRET_KEY);
  } 
}

const createAndSendMail = async (email) => {
  const credential = await Credential.findOne({email: email});
  let mailTransporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'epamsmtpnext@gmail.com',
      pass: 'rojeck1409'
    }
  });
  let mailDetails = {
    from: 'epamsmtpnext@gmail.com',
    to: `${email}`,
    subject: 'Email with pass',
    text: `Your password: ${credential.password}`
  }

  mailTransporter.sendMail(mailDetails, function(error, info) {
    if(error) {
      console.log(error);
    } else {
      console.log(info.response);
    }
  })
}

module.exports = {
  saveUser,
  checkPassword,
  createAndSendMail
}