const jwt = require('jsonwebtoken');

const SECRET_KEY = 'alwaysclose';

const authMiddleware = (req, res, next) => {
  const {authorization} = req.headers;

  if(!authorization) {
    return res.status(400).json({"message": "Provide autorization data"});
  }

  const [ , token] = authorization.split(' ');

  if(!token) {
    return res.status(400).json({"message": "Please, include token to request"});
  }

  try {
    const tokenPayload = jwt.verify(token, SECRET_KEY);
    req.user = {
      email: tokenPayload.email,
      userId: tokenPayload.userId,
      role: tokenPayload.role
    }
    next();
  }catch (err) {
    return res.status(400).json({message: err.message});
  }
}

module.exports = {
  authMiddleware
}